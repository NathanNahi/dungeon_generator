Dungeon Generator
-----------------------------------

Requirements

- node
- tsc
- web server

-----------------------------------

How to use

- Open terminal of your choosing
- Run the "node build.js --build" in the current directory
- Run your web server in the current directory
- Open default.html in the browser of your choosing